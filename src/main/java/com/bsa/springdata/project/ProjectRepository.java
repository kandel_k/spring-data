package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

//    @Query(value = "SELECT p.* " +
//            "FROM teams t JOIN projects p ON t.project_id = p.id " +
//            "JOIN technologies tech on t.technology_id = tech.id " +
//            "JOIN users u on t.id = u.team_id " +
//            "WHERE tech.name = :technology " +
//            "GROUP BY p.id " +
//            "ORDER BY COUNT(*) " +
//            "LIMIT 5",
//            nativeQuery = true)
    @Query("SELECT p " +
            "FROM Team t JOIN t.project p " +
            "JOIN t.users u " +
            "JOIN t.technology tech " +
            "WHERE tech.name = :technology " +
            "GROUP BY p.id " +
            "ORDER BY COUNT(p.id)")
    List<Project> findTop5ByTechnology(@Param("technology") String technology, Pageable pageable);

//    @Query(value = "SELECT p.* " +
//            "FROM teams t JOIN projects p on t.project_id = p.id " +
//            "JOIN users u ON u.team_id = t.id " +
//            "GROUP BY p.id, p.name " +
//            "ORDER BY COUNT(distinct t.id) DESC, COUNT(u.id) DESC, p.name DESC " +
//            "LIMIT 1",
//            nativeQuery = true)
    @Query("SELECT p " +
            "FROM Team t JOIN t.project p " +
            "JOIN t.users u " +
            "GROUP BY p.id, p.name " +
            "ORDER BY COUNT(distinct t.id) DESC, COUNT(u.id) DESC, p.name DESC")
    Page<Project> findTheBiggest(Pageable pageable);

//    @Query(value = "SELECT COUNT(distinct p) " +
//            "FROM teams t JOIN projects p on t.project_id = p.id " +
//            "JOIN users u on t.id = u.team_id " +
//            "JOIN user2role u2r on u.id = u2r.user_id " +
//            "JOIN roles r on u2r.role_id = r.id " +
//            "WHERE r.name = :role",
//            nativeQuery = true)
    @Query("SELECT COUNT(DISTINCT p) " +
            "FROM Team t JOIN t.project p " +
            "JOIN t.users u " +
            "JOIN u.roles r " +
            "WHERE r.name = :role")
    int getCountWithRole(@Param("role") String role);

    @Query(value = "SELECT p.name , " +
            "COUNT(DISTINCT t.id) as teamsNumber, " +
            "COUNT(u.id) as developersNumber, " +
            "STRING_AGG(DISTINCT t2.name, ',') as tech " +
            "FROM teams t JOIN users u on t.id = u.team_id " +
            "JOIN technologies t2 on t.technology_id = t2.id " +
            "JOIN projects p on t.project_id = p.id " +
            "GROUP BY p.id ",
            nativeQuery = true)
    List<ProjectSummaryDto> getSummary();
}
