package com.bsa.springdata.project;

import com.bsa.springdata.entity.BaseEntity;
import com.bsa.springdata.team.Team;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Builder
@Entity
@Table(name = "projects")
public class Project extends BaseEntity {

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "project")
    private List<Team> teams;
}
