package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository.findTop5ByTechnology(technology, PageRequest.of(0,5)).stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        return projectRepository.findTheBiggest(PageRequest.of(0,1))
                .get()
                .map(ProjectDto::fromEntity)
                .findAny();
    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
        return projectRepository.getCountWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto requestDto) {
        var technology = Technology.builder()
                .description(requestDto.getTechDescription())
                .name(requestDto.getTech())
                .link(requestDto.getTechLink())
                .build();
        var project = Project.builder()
                .description(requestDto.getProjectDescription())
                .name(requestDto.getProjectName())
                .build();
        var team = Team.builder()
                .area(requestDto.getTeamArea())
                .name(requestDto.getTeamName())
                .room(requestDto.getTeamRoom())
                .project(project)
                .technology(technology)
                .build();

        technologyRepository.save(technology);
        var id = projectRepository.save(project).getId();
        teamRepository.save(team);
        return id;
    }
}
