package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameIgnoreCaseContaining(String lastName, Pageable pageable);

    List<User> findByOfficeCityOrderByLastNameAsc(String city);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    List<User> findByTeamRoomAndOfficeCity(String room, String city, Sort sort);

    int deleteByExperienceLessThan(int experience);

}
