package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Transactional
    @Modifying
//    @Query(value = "DELETE " +
//            "FROM roles " +
//            "WHERE roles.id = (SELECT DISTINCT r.id " +
//            "FROM roles r LEFT JOIN user2role u2r on r.id = u2r.role_id " +
//            "WHERE u2r.user_id is NULL AND r.code = :code)",
//            nativeQuery = true)
    @Query("DELETE " +
            "FROM Role r " +
            "WHERE r.id = (SELECT DISTINCT role.id " +
            "FROM Role role LEFT JOIN role.users u " +
            "WHERE u.id IS NULL AND role.code = :code)")
    void deleteRolesByName(@Param("code") String code);
}
