package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TechnologyRepository extends JpaRepository<Technology, UUID> {

    Optional<Technology> findByName(String name);

//    @Query(value = "SELECT t.* " +
//            "FROM technologies t JOIN teams team on t.id = team.technology_id " +
//            "WHERE t.name = :name AND team.id IN (:ids)",
//            nativeQuery = true)
    @Query("SELECT t " +
            "FROM Team team JOIN team.technology t " +
            "WHERE t.name = :name AND team.id IN (:ids)")
    List<Technology> findByTeamIdInAndName(@Param("ids") List<UUID> ids, @Param("name") String name);
}
