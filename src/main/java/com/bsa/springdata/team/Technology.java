package com.bsa.springdata.team;

import com.bsa.springdata.entity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "technologies")
public class Technology extends BaseEntity {

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String link;
}
