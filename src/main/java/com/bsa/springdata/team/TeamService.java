package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        var ids = teamRepository.findByUsersCount(devsNumber).stream()
                .map(Team::getId)
                .collect(Collectors.toList());
        technologyRepository.findByTeamIdInAndName(ids, oldTechnologyName)
                .forEach(technology -> {
                    technology.setName(newTechnologyName);
                    technologyRepository.save(technology);
                });
    }

    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }
}
