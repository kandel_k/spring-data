package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Transactional
    @Modifying
    @Query(value = "UPDATE teams t " +
            "SET name = res.name " +
            "FROM (SELECT team.id, team.name || '_' || p.name || '_' || tech.name as name " +
            "FROM teams team JOIN projects p " +
            "ON team.project_id = p.id " +
            "JOIN technologies tech " +
            "ON team.technology_id = tech.id " +
            "WHERE team.name = :name) res " +
            "WHERE t.id = res.id",
            nativeQuery = true)
    void normalizeName(@Param("name") String name);

//    @Query(value = "SELECT t.* " +
//            "FROM teams t JOIN users u on t.id = u.team_id " +
//            "GROUP BY t.id " +
//            "HAVING COUNT(*) < :users",
//            nativeQuery = true)
    @Query("SELECT t " +
            "FROM Team t JOIN t.users u " +
            "GROUP BY t.id " +
            "HAVING COUNT(u.id) < :users")
    List<Team> findByUsersCount(@Param("users") long users);

    Optional<Team> findByName(String name);

    int countByTechnologyName(String name);
}
