package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
//    @Query(value = "SELECT DISTINCT o.* " +
//            "FROM offices o JOIN users u ON o.id = u.office_id " +
//            "JOIN teams t ON u.team_id = t.id " +
//            "JOIN technologies tech ON t.technology_id = tech.id " +
//            "WHERE tech.name = :technology",
//            nativeQuery = true)
    @Query("SELECT distinct o " +
            "FROM Office o JOIN o.users u JOIN u.team t JOIN t.technology tech " +
            "WHERE tech.name = :technology")
    List<Office> getOfficesByTechnology(@Param("technology") String technology);

//    @Query(value = "SELECT DISTINCT o.* " +
//            "FROM offices o JOIN users u ON o.id = u.office_id " +
//            "WHERE o.address = :address ",
//            nativeQuery = true)
    @Query("SELECT o " +
            "FROM Office o JOIN o.users " +
            "WHERE o.address = :address")
    Optional<Office> findByAddress(@Param("address") String address);
}
