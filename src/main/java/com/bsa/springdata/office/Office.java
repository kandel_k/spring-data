package com.bsa.springdata.office;

import com.bsa.springdata.entity.BaseEntity;
import com.bsa.springdata.user.User;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Builder
@Table(name = "offices")
public class Office extends BaseEntity {

    @Column
    private String city;

    @Column
    private String address;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "office")
    private List<User> users;
}
